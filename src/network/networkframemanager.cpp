﻿#include "../../includes/network/networkframemanager.h"

#include "QtWebSockets/qwebsocket.h"
#include "../../includes/network/cwebsocketserver.h"

/**
 * @brief NetworkFrameManager::OnProcessConnectedNetMes 处理一个新的连接到达
 * @param conn 到达的新的连接
 */
void NetworkFrameManager::OnProcessConnectedNetMes(QWebSocket *conn)
{    

}

/**
 * @brief NetworkFrameManager::OnProcessDisconnectedNetMes 处理一个连接关闭
 * @param conn 要断开的连接
 */
void NetworkFrameManager::OnProcessDisconnectedNetMes(QWebSocket *conn)
{

}

/**
 * @brief NetworkFrameManager::OnProcessNetBinary 处理网络二进制消息
 * @param conn 要处理的客户端
 * @param data 到达的二进制消息
 */
void NetworkFrameManager::OnProcessNetBinary(QWebSocket *conn,QByteArray &data)
{

}

/**
 * @brief NetworkFrameManager::OnProcessNetText 处理网络字符串消息
 * @param conn 要处理的客户端
 * @param mes 到达的字符串消息
 */
void NetworkFrameManager::OnProcessNetText(QWebSocket *conn,QString mes)
{

}

/**
 * @brief NetworkFrameManager::OnProcessSendFile 处理文件发送（客户端使用）
 * @param conn 要处理的客户端
 * @param file 要处理的文件的绝对路径
 * @param sendsize 当前发送的文件数据大小
 * @param totalsize 当前文件总的大小
 */
void NetworkFrameManager::OnProcessSendFile(QWebSocket *conn,QString file,qint64 sendsize,qint64 totalsize)
{

}

/**
 * @brief NetworkFrameManager::OnProcessRecvFile 处理文件接收
 * @param conn 要处理的客户端 
 * @param srcfile 原始文件名称
 * @param decfile 保存的文件
 * @param pFileRecvError 状态码
 */
void NetworkFrameManager::OnProcessRecvFile(QWebSocket *conn,QString srcfile,QString decfile,FileRecvError pFileRecvError)
{

}

