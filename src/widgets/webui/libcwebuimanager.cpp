﻿#include "../../../includes/widgets/libcwebuimanager.h"
#include "../../../includes/QsLog/QsLog.h"

#include <QLayout>

CWebUIManager::CWebUIManager(QObject *parent)
{
    m_webChannel.registerObject("CWebUIManager",this);
}

CWebUIManager::~CWebUIManager()
{

}

/**
 * @brief CWebUIManager::OpenUrl 打开一个url
 *
 * @param pLayout 用于显示我们打开的网页控件
 * @param url 要打开的url
 * @param isShow 是否要显示，初始是一打开就显示
 * @param isContentMenu 是否要使用右键菜单
 */
void CWebUIManager::openUrl(QLayout *pLayout,QString url,bool isShow,bool isContentMenu)
{
    if(pLayout == NULL || url.isEmpty())
        return;

    QLOG_INFO()<<"CWebUIManager::openUrl:"<<url;

    pLayout->addWidget(&m_webView);
    m_webView.load(QUrl(url));
    m_webView.page()->setWebChannel(&m_webChannel);

    if(!isContentMenu) m_webView.setContextMenuPolicy(Qt::NoContextMenu);
    if(isShow) m_webView.show();
}

/**
 * @brief CWebUIManager::setShow 是否显示网页
 * @param isShow 是否要显示网页控件
 */
void CWebUIManager::setShow(bool isShow)
{
    if(isShow) m_webView.show();
    else m_webView.hide();
}

/**
 * @brief reLoadData 重新载入网页
 */
void CWebUIManager::reLoadData(void)
{
    m_webView.reload();
}

/**
 * @brief CWebUIManager::sendDataToQt 处理从网页中发送过来的数据
 *
 * @param data 网页发送过来的数据
 */
void CWebUIManager::sendDataToQt(const QString data)
{
    if(data.isEmpty())
        return;

    QLOG_INFO()<<"CWebUIManager::sendDataToQt:"<<data;

    emit UrlSendedData(data);
}

/**
 * @brief CWebUIManager::sendDataToUrl 发送数据给网页
 * @param data 要给网页的数据
 */
void CWebUIManager::sendDataToUrl(const QString data)
{
    if(data.isEmpty())
        return;

    QLOG_INFO()<<"CWebUIManager::sendDataToUrl:"<<data;

    emit onProcessQtSendedData(data);
}
