﻿#include "../../includes/common/common.h"
#include "../../includes/QsLog/QsLog.h"
#include "../../includes/breakpad/exception_handler.h"

#include <QDir>

using namespace QsLogging;

bool dump_callback(const wchar_t *dump_path, const wchar_t *id, void *content, EXCEPTION_POINTERS *exinfo, MDRawAssertionInfo *assertion, bool succeeded)
{
    if(succeeded)
    {
        QLOG_ERROR()<<"系统已经崩溃,dmp已经生成成功,请联系开发人员!";
    }
    else
    {
        QLOG_ERROR()<<"系统已经崩溃,dmp生成失败,请联系开发人员!";
    }

    return succeeded;
}

/**
 * @brief init_lib_resources 加载本静态库的资源
 */
void init_lib_resources(void)
{
    Q_INIT_RESOURCE(libqtcore);
}

/**
 * @brief clean_lib_resources 卸载本静态库的资源
 */
void cleanup_lib_resources(void)
{
    Q_CLEANUP_RESOURCE(libqtcore);
}

/**
 * @brief init_log_file 生成日志文件
 * @param filepath 要生成的日志文件路径
 */
void init_log_file(QString filepath)
{
    if(filepath.isEmpty())
        return;

    // 初始化日志机制
    Logger& logger = Logger::instance();
    logger.setLoggingLevel(QsLogging::TraceLevel);

    // 添加文件为目的地
    const QString sLogPath(QDir(QApplication::applicationDirPath()).filePath(filepath));
    DestinationPtr fileDestination(DestinationFactory::MakeFileDestination(
      sLogPath, EnableLogRotation, MaxSizeBytes(512*1024), MaxOldLogCount(5)));
    logger.addDestination(fileDestination);

    QLOG_INFO()<<"log system init success.";
}

/**
 * @brief sava_file 保存源文件到目标文件(主要网络传输中使用)
 * @param srcfile 要保存的源文件的路径(相对路径)
 * @param decfile 目标文件路径(绝对路径)
 *
 * @return 如果文件保存成功返回真，否则返回假
 */
bool sava_file(QString srcfile,QString decfile)
{
    bool preturnState = true;

    QString appfiledir = decfile;
    QString tmpFileDirPath = appfiledir.mid(0,appfiledir.lastIndexOf("/"));

    QDir dir(tmpFileDirPath);
    if(!dir.exists())
    {
        if(!dir.mkpath(tmpFileDirPath))
        {
            QLOG_ERROR()<<"sava_file:"<<tmpFileDirPath<<" create fail.";
            preturnState=false;
        }
    }

    QFileInfo pFileInfo(appfiledir);
    if(pFileInfo.exists())
    {
        if(!QFile::remove(appfiledir))
        {
            QLOG_ERROR()<<"sava_file:"<<appfiledir<<" remove fail.";
            preturnState=false;
        }
    }

    if(!QFile::copy(srcfile,appfiledir))
    {
       QLOG_ERROR()<<"sava_file:"<<appfiledir<<" copy fail.";
       preturnState=false;
    }

    return preturnState;
}

/**
 * @brief init_dump_system 初始化崩溃系统
 */
void init_dump_system(void)
{
    google_breakpad::ExceptionHandler eh(
        L".", NULL, dump_callback, NULL, google_breakpad::ExceptionHandler::HANDLER_ALL);
}

