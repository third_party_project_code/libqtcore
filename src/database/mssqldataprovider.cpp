﻿#include "../../includes/database/mssqldataprovider.h"
#include "../../includes/database/ndbpool.h"
#include "../../includes/QsLog/QsLog.h"

#include <QSqlRecord>

initialiseSingleton(MsSqlDataProvider);

/** 
 * 构造函数
 */
MsSqlDataProvider::MsSqlDataProvider(QObject *parent)
    : QObject(parent),m_dbPort(0)
{

}

/** 
 * 析构函数
 */
MsSqlDataProvider::~MsSqlDataProvider(void)
{
    disconnect();
}

/** 
 * 建立与数据库的连接
 *
 * @param host 要连接的数据库的IP地址
 * @param username 要连接数据库的用户名
 * @param password 要连接数据库的用户密码
 * @param dbName 要连接的数据库名称
 * @param port 数据库断开
 */
bool MsSqlDataProvider::connect(QString host,QString username,QString password,QString dbName,int port)
{
    m_dbhost = host;
    m_dbusername = username;
    m_dbpassword = password;
    m_dbname = dbName;
    m_dbPort = port;

	return true;
}

/**  
 * 执行SQL语句
 *
 * @param sql 要执行的SQL语句
 *
 * @return 如果成功获取到数据返回这个数据记录，否则抛出异常
 */
const RecordSetList MsSqlDataProvider::execSql(const QString& sql)
{
	RecordSetList pRecordSetList;

    QSqlDatabase tempDB = NDBPool::getNewConnection(m_dbhost,m_dbname,m_dbusername,m_dbpassword,m_dbPort,true);
    QLOG_INFO() << " connection name:" << tempDB.connectionName() << "is vaild:" << tempDB.isOpen() << "\n";

    if(tempDB.isOpen())
    {
        QSqlQuery queryresult(tempDB);

        if(queryresult.exec(sql))
        {
            try {
                do
                {
                    RecordSet pRecordSet;
                    QSqlRecord precord = queryresult.record();

                    Row fieldNames;
                    for(int i=0;i<precord.count();i++)
                    {
                        //qDebug()<<"query:"<<precord.fieldName(i);
                        fieldNames.push_back(precord.fieldName(i));
                    }

                    pRecordSet.setColumnHeaders(fieldNames);

                    while(queryresult.next())
                    {
                        Row fieldDatas;
                        for(int i=0;i<precord.count();i++)
                        {
                            fieldDatas.push_back(queryresult.value(i).toString());
                        }

                        pRecordSet.add(fieldDatas);
                    }

                    pRecordSetList.add(pRecordSet);
                }
                while(queryresult.nextResult());
            } catch (...) {
                QLOG_ERROR()<<"query error:"<<queryresult.lastError().text();
            }
        }
    }

    //NDBPool::closeConnection(tempDB);

	return pRecordSetList;
}

/** 
 * 关闭与数据库的连接
 */
void MsSqlDataProvider::disconnect(void)
{
    //NDBPool::release();
}

