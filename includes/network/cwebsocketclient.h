﻿#ifndef CWEBSOCKETCLIENT_H
#define CWEBSOCKETCLIENT_H

#include <QObject>
#include <QTimer>
#include <QtCore/QHash>
#include <QtCore/QByteArray>
#include <QWebSocket>
#include <QTimer>
#include <QUrl>
#include <QFile>

#include "../common/singleton.h"
#include "../common/common.h"
#include "networkframemanager.h"

QT_FORWARD_DECLARE_CLASS(QWebSocket)

class CWebSocketClient : public QObject
{
    Q_OBJECT

public:
    explicit CWebSocketClient(NetworkFrameManager *pNetworkFrameManager=NULL,QObject *parent = nullptr);
    ~CWebSocketClient();

    /// 打开指定地址的网络连接
    Q_INVOKABLE void Open(QUrl url);
    /// 关闭连接
    Q_INVOKABLE void Close(void);
    /// 发送字符串
    Q_INVOKABLE qint64 Send(QString msg);
    /// 发送文件(注意点：在网络中使用时一定要设置isExcludeUserInputEvents为false,这个参数是在界面中做防假卡死的)
    Q_INVOKABLE bool sendFile(QString filepath,bool isExcludeUserInputEvents=true,QString rootpath="");
    /// 发送二进制数据
    Q_INVOKABLE qint64 sendBinaryMessage(const QByteArray &data);
    /// 设置网络消息处理框架
    Q_INVOKABLE void SetNetworkFrameManager(NetworkFrameManager *pNetworkFrameManager);
    /// 是否处理接收文件
    Q_INVOKABLE void setIsProcessRecvFile(bool isProcess,QString recvfiledir="");

Q_SIGNALS:
    void closed();

private Q_SLOTS:
    /// 处理字符串消息
    void onTextMessageReceived(QString message);
    /// 处理二进制消息
    void onBinaryReceived(QByteArray message);
    /// 处理连接关闭
    void onDisconnected();
    /// 处理连接成功
    void onConnected();
    /// 处理心跳
    void handleWebSocketHeartTimeOut();
    /// 处理断开重连
    void handleWebSocketReconnect();

private:
    /// 处理文件接收
    void onPrcessRecvFile(QWebSocket *pwebsocket,const QByteArray &data);

private:
    QWebSocket m_webSocket;                                             /**< 网络客户端 */
    QUrl m_websocketurl;                                                /**< 要连接的服务器地址 */
    QTimer m_WebSocketHeartTimeOutTimer,m_WebSocketReconnectTimer;      /**< 用于处理心跳和断开重连的定时器 */

    NetworkFrameManager *m_NetworkFrameManager;                         /**< 网络消息处理框架 */

    QByteArray m_recvFileBytes;                                         /**< 用于处理接收到的文件数据 */
    bool m_recvFileState;                                               /**< 接收文件的状态控制 */
    tagFileStruct m_tagFileStruct;                                      /**< 用于文件接收处理 */
    bool m_processrecvFile;                                             /**< 是否处理接收文件 */
    QString m_recvFileSaveDir;                                          /**< 接收文件的保存路径 */
};

class CWebSocketClientManager : public Singleton<CWebSocketClientManager>
{
public:
     CWebSocketClientManager();
     ~CWebSocketClientManager();

     /// 添加一个客户端
     bool addClient(QString clientName,CWebSocketClient* pClient);
     /// 得到一个客户端
     CWebSocketClient* getClient(QString clientName);
     /// 清除所有的客户端
     void clearAllClients(void);
     /// 清除指定的客户端
     bool deleteClient(QString clientName);

private:
    QHash<QString,CWebSocketClient*> m_WebSocketClients;
};

#define sCWebSocketClientManager CWebSocketClientManager::getSingleton()

#endif // CWEBSOCKETCLIENT_H
