﻿#ifndef NETWORK_FRAME_MANAGER_H
#define NETWORK_FRAME_MANAGER_H

#include <QObject>
#include <QString>
#include <QByteArray>

#include "../common/common.h"

QT_FORWARD_DECLARE_CLASS(QWebSocket)

class NetworkFrameManager
{
public:
    /// 处理网络字符串消息
    virtual void OnProcessNetText(QWebSocket *conn,QString mes);
    /// 处理网络二进制消息
    virtual void OnProcessNetBinary(QWebSocket *conn,QByteArray &data);
    /// 处理一个新的连接到达
    virtual void OnProcessConnectedNetMes(QWebSocket *conn);
    /// 处理一个连接关闭
    virtual void OnProcessDisconnectedNetMes(QWebSocket *conn);
    /// 处理文件发送
    virtual void OnProcessSendFile(QWebSocket *conn,QString file,qint64 sendsize,qint64 totalsize);
    /// 处理文件接收
    virtual void OnProcessRecvFile(QWebSocket *conn,QString srcfile,QString decfile,FileRecvError pFileRecvError);
};

#endif // SERVERFRAMEMANAGER_H
