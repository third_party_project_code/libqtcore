﻿#ifndef _C_UDP_SOCKET_H_INCLUDE_
#define _C_UDP_SOCKET_H_INCLUDE_

#include "../../includes/common/common.h"

#include <QObject>
#include <QUdpSocket>

class CUdpSocket : public QObject
{
    Q_OBJECT

public:
    explicit CUdpSocket(QObject *parent = nullptr);
    ~CUdpSocket(void);

    /// 开始指定端口开始侦听
    Q_INVOKABLE bool Open(int port);
    /// 发送报文
    Q_INVOKABLE qint64 Send(QByteArray Datagramdata,int port,QHostAddress type=QHostAddress::Broadcast);

signals:
    void processPendingDatagram(QByteArray datagramdata);

private slots:
    void reciverPendingDatagram();

private:
    QUdpSocket m_UdpSocket;
    QByteArray m_datagramData;

    tagDatagramHearder m_DatagramHearder;
    bool m_isProcessDatagramHearder;
};

#endif
