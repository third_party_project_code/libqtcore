#ifndef _MSSQL_DATA_PROVIDER_H_INCLUDE
#define _MSSQL_DATA_PROVIDER_H_INCLUDE

#include "recordset.h"
#include "../common/singleton.h"

#include <QObject>
#include <QString>

/** 
 * mssql的数据库操作实现类
 */
class MsSqlDataProvider : public QObject, public Singleton<MsSqlDataProvider>
{
    Q_OBJECT

public:
    /// 构造函数
    MsSqlDataProvider(QObject *parent = nullptr);
	/// 析构函数
    ~MsSqlDataProvider(void);

	/// 建立与数据库的连接
    Q_INVOKABLE bool connect(QString host,QString username,QString password,QString dbName,int port=1433);
	/// 执行SQL语句
    Q_INVOKABLE const RecordSetList execSql(const QString& sql);
	/// 关闭与数据库的连接
    Q_INVOKABLE void disconnect(void);

private:
    QString m_dbhost;              /**< 数据库ip */
    QString m_dbusername;          /**< 用户名 */
    QString m_dbpassword;          /**< 用户密码 */
    QString m_dbname;              /**< 数据库名称 */
    int m_dbPort;                  /**< 数据库端口 */
};

#define sMsSqlDataProvider MsSqlDataProvider::getSingleton()

#endif
