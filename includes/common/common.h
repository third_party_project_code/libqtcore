﻿#ifndef COMMON_H
#define COMMON_H

#include <QApplication>

#define BUF_SIZE 1024*4
#define MAX_FILENAME 256

/**
 * @brief The tagFileStruct struct 要发送的文件结构
 */
#pragma pack(push,1)
struct tagFileStruct
{
    char fileName[MAX_FILENAME];            // 文件所在相对路径
    qint64 srcfileSize;            // 原始文件大小
    qint64 compressfileSize;       // 压缩后文件大小
    quint16 checknum;              // 数据校验码
};
#pragma pack(pop)

/**
 * @brief 要发送udp报文头
 */
#pragma pack(push,1)
struct tagDatagramHearder
{
    int version;
    qint64 size;                   // 数据大小
};
#pragma pack(pop)

/**
 * @brief The FileRecvError enum 文件接收错误返回
 */
enum FileRecvError
{
    STATE_CHECKNUM = 0,        // 文件校验码不对
    STATE_SIZE,                // 文件长度不对
    STATE_NOTWRITE,            // 路径不对，无法写入
    STATE_SUCCESS,             // 接收成功
    STATE_START,               // 开始接收
    STATE_NULL
};

/// 加载本静态库的资源
void init_lib_resources(void);
/// 卸载本静态库的资源
void cleanup_lib_resources(void);
/// 生成日志文件
void init_log_file(QString filepath);
/// 保存源文件到目标文件(主要网络传输中使用)
bool sava_file(QString srcfile,QString decfile);
/// 初始化崩溃系统
void init_dump_system(void);

#endif // COMMON_H
