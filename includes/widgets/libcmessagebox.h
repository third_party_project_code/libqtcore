﻿#ifndef LIBCMESSAGEBOX_H
#define LIBCMESSAGEBOX_H

#include <QDialog>

#include "libframelesswindow.h"

namespace Ui {
class libcmessagebox;
}

namespace CMessageBox
{

class libcmessagebox : public FramelessWindow
{
    Q_OBJECT

public:
    explicit libcmessagebox(QWidget *parent = nullptr);
    ~libcmessagebox();

    /// 警告
    Q_INVOKABLE int waring(QString title,QString content);
    /// 信息
    Q_INVOKABLE int information(QString title,QString content);
    /// 问题
    Q_INVOKABLE int question(QString title,QString content);

private slots:
    void on_pushButton_close_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();

private:
    Ui::libcmessagebox *ui;
};

/// 警告
int Waring(QWidget *parent,QString title,QString content,bool isMask=false);
/// 信息
int Information(QWidget *parent,QString title,QString content,bool isMask=false);
/// 问题
int Question(QWidget *parent,QString title,QString content,bool isMask=false);

}

#endif // LIBCMESSAGEBOX_H
